package com.springernature.search.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PageBase {

    protected final WebDriver wd;

    public PageBase (WebDriver wd) {
        this.wd = wd;
    }

    public void goTo(String url) {
        wd.get(url);
    }

    public void type(By locator, String input) {
        click(locator);
        wd.findElement(locator).clear();
        wd.findElement(locator).sendKeys(input);
    }

    public void click(By locator) {
        wd.findElement(locator).click();
    }

    public boolean isElementPresent(By locator) {
        try {
            wd.findElement(locator);
            return true;
        } catch (org.openqa.selenium.NoSuchElementException ex){
            return false;
        }
    }

}
