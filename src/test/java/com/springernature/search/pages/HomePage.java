package com.springernature.search.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends PageBase {

    private By SEARCH_ICON = By.cssSelector("#search");
    private By SEARCH_INPUT_FIELD = By.cssSelector("#query");

    public HomePage(WebDriver wd) {
        super (wd);
        goTo("https://link.springer.com/");
    }

    public ResultPage search (String searchTerm){
        type(SEARCH_INPUT_FIELD, searchTerm);
        click(SEARCH_ICON);
        return new ResultPage(wd);
    }
}
