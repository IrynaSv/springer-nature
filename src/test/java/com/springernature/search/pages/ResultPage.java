package com.springernature.search.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class ResultPage extends PageBase{

    private By TITLE = By.cssSelector("#title");
    private By NO_RESULTS_MESSAGE = By.cssSelector("#no-results-message");

    public ResultPage(WebDriver wd) {
        super (wd);
    }

    public List<String> getResultsTitles() {

        List<String> resultTitles = new ArrayList<>();
        List<WebElement> titles = wd.findElements(TITLE);
        for (WebElement title : titles) {
            String resultTitle = title.getText();
            resultTitles.add(resultTitle);
        }
        return resultTitles;

    }

    public boolean noResultsFound() {
        return isElementPresent(NO_RESULTS_MESSAGE);
    }
}
