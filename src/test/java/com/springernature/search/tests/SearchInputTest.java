package com.springernature.search.tests;


import com.springernature.search.pages.HomePage;
import com.springernature.search.pages.ResultPage;
import org.testng.annotations.Test;
import java.util.List;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsStringIgnoringCase;
import static org.hamcrest.Matchers.equalTo;


public class SearchInputTest extends TestBase {

    @Test
    public void testValidSearchResultsContainSearchTerm() {
        HomePage homePage = new HomePage(wd);
        String searchTerm = "Physics";
        ResultPage resultPage = homePage.search(searchTerm);
        List<String> titles = resultPage.getResultsTitles();
        for (String title : titles){
            assertThat(title, containsStringIgnoringCase(searchTerm));
        }
    }


    @Test
    public void testInvalidSearchTerm() {
        HomePage homePage = new HomePage(wd);
        String searchTerm = "jhfjhghgfhgsdfsf";
        ResultPage resultPage = homePage.search(searchTerm);
        assertThat(resultPage.noResultsFound(), equalTo(true));
    }

}
