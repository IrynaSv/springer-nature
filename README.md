### What is this repository for? ###

* The repository contains automated tests for Search input field on https://link.springer.com/ in Chrome.
* The tests are written in Java using Selenium web driver and TestNG framework.
* There are two tests: one positive (checking that the search results contain the search term) and one negative (checking that "No results found" page is returned for an invalid search term)

### How do I get set up? ###

* You should have Google Chrome browser installed.
* To launch tests run SearchInputTest class.
